import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class VeriffyActualTitle {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");
        //System.setProperty("webdriver.chrome.driver","C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\chromedriver.exe");
        //WebDriver driver = new ChromeDriver();
        WebDriver driver = new FirefoxDriver();
        String baseUrl = "http://demo.guru99.com/test/newtours/";
        String expectedTitle = "Welcome: Mercury Tour";

        String actualTitle = "";

        //launch Firefox and direct it to the Base URL
        driver.get(baseUrl);

        // get the actual value of the title
        actualTitle=driver.getTitle();


        //compare the actual title with the expected one and print the result "Passed" or  "Faled"

        if(actualTitle.contentEquals(expectedTitle)){
            System.out.println("Test Passed");
        }
        else {
            System.out.println("Test Failed");
        }

        //close FireFox
        driver.close();
    }
}
