import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ClickOnImage {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");

        WebDriver driver = new FirefoxDriver();

        String baseUrl = "https://www.facebook.com/login/identify?ctx=recover";
        driver.get(baseUrl);

        driver.findElement(By.cssSelector("a[title=\"Go to Facebook Home\"]"));

        //verify that we are now back on fc homepage
        if(driver.getTitle().equals("Facebook - log in or sign up")){
            System.out.println("we are back at facebook homepage");
        }
        else {
            System.out.println(" we are not on fc homepage");
        }
        driver.close();
    }
}
