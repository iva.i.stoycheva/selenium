import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDownMenu {
    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        String baseURL = "http://demo.guru99.com/test/newtours/register.php";
        driver.get(baseURL);

        Select drpCountry = new Select(driver.findElement(By.name("country")));
        drpCountry.selectByVisibleText("ANTARCTICA");

        //Selecting Items in a Multiple SELECT elements
        driver.get("http://jsbin.com/osebed/2");
        Select fruits = new Select(driver.findElement(By.id("fruits")));
        fruits.selectByVisibleText("Banana");
        fruits.selectByIndex(1);


    }
}
