import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LocatingGUIElements {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        String baseUrl = "http://www.facebook.com";
        String tagName = "";

        driver.get(baseUrl);
        tagName=driver.findElement(By.id("email")).getTagName();
        System.out.println(tagName);

        //close single browser
        driver.close();
    }
}
