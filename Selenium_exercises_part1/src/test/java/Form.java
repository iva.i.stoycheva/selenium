import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Form {
    public static void main(String[] args) {
        //declaration and instantiation of objects/variables

        System.setProperty("webdriver.gecko.driver", "C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");

        WebDriver driver = new FirefoxDriver();
        String baseUrl = "http://demo.guru99.com/test/login.html";
        driver.get(baseUrl);

        //get WeElement correspond  to the email field
        WebElement email = driver.findElement(By.id("email"));

        //get Weblelemet cprrespond to the password field
        WebElement password = driver.findElement(By.name("passwd"));

        email.sendKeys("abcd@gmail.com");
        password.sendKeys("abcdefghlkjl");

        //delete values in the text box
        email.clear();
        password.clear();
        System.out.println("Text Field Cleared");

        //find the submit button
        WebElement login = driver.findElement(By.id("SubmitLogin"));


        // Using click method to submit form
        email.sendKeys("abcd@gmail.com");
        password.sendKeys("abcdefghlkjl");
        login.click();
        System.out.println("Login Done with click");

        //using submit method to submit the form. Submit used on password field
        driver.get(baseUrl);
        driver.findElement(By.id("email")).sendKeys("abcd@gmail.com");
        driver.findElement(By.name("passwd")).sendKeys("abcdefghlkjl");
        driver.findElement(By.id("SubmitLogin")).submit();
        System.out.println("Login Done with submit");

        //driver.close();

    }
}
