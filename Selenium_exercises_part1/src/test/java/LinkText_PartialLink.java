import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LinkText_PartialLink {
    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();

        String baseUrl = "http://demo.guru99.com/test/link.html";
        driver.get(baseUrl);

        driver.findElement(By.linkText("click here")).click();
        System.out.println("title of page is " + driver.getTitle());
        driver.quit();


    }
}
