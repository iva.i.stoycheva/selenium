import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SwitchingBetweenPop_upWindows {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe" );
        WebDriver driver = new FirefoxDriver();

        String allertMessage = " ";

        driver.get("http://jsbin.com/usidix/1");

        driver.findElement(By.cssSelector("input[value=\"Go!\"]")).click();
        allertMessage = driver.switchTo().alert().getText();
        System.out.println(allertMessage);
        driver.quit();
    }
}
