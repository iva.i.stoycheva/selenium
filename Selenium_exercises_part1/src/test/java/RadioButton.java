import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RadioButton {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");

        WebDriver driver = new FirefoxDriver();

        String baseUrl = "http://demo.guru99.com/test/radio.html";

        driver.get(baseUrl);

        WebElement radio1 = driver.findElement(By.id("vfb-7-1"));

        WebElement radio2 = driver.findElement(By.id("vfb-7-2"));

        //radio1 is select
        radio1.click();
        System.out.println("Radio button option 1 selected");

        //radio2 is selected
        radio2.click();

        //Selecting Checkbox

        WebElement option1 = driver.findElement(By.id("vfb-6-0"));
        option1.click();

        if(option1.isSelected()){
            System.out.println("CheckBOxIs Toggled");
        }
        else {
            System.out.println("CheckBox is toggled of");
        }

        // selecting checkbox and using isSelected Method
        driver.get("http://demo.guru99.com/test/facebook.html");

        WebElement checkFBPersist = driver.findElement(By.id("persist_box"));

        for (int i = 0; i < 2 ; i++) {
            checkFBPersist.click();
            System.out.println(("Facebook Persists CheckBox Status is ") + checkFBPersist.isSelected());
        }
        //driver.close();
    }
}
