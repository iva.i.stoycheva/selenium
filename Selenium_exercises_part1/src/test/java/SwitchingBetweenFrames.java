import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SwitchingBetweenFrames {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\buy5_\\OneDrive\\Desktop\\QA_July\\qa_finalproject_team_july\\Selenium_exercises_part1\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://demo.guru99.com/selenium/deprecated.html");
        driver.switchTo().frame("classFrame");
        driver.findElement(By.linkText("Deprecated")).click();
        driver.close();
    }
}
